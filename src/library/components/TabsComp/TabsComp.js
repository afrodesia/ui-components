import React, { Component } from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs'
import styled from 'styled-components'

//  Styles 

const TabStyle = styled.div`
  width:100%;
  .react-tabs__tab-list{
    float:left;
    width:100%;
    li{
      display:inline-block;
      padding:20px;
      font-weight:700;
      cursor: pointer;
      background:#f8f8f8;
    }
  }
  section{
    background:#f8f8f8;
    clear:both;
    padding:20px;
    h2{
      padding-bottom:20px;
      margin:0px;
    }
  }
  @media only screen and (max-width: 800px){

  }
`;

// Components 

// import { SignUp, SignIn } from './'

class TabsComp extends Component{
  constructor() {
    super();
    this.state = { tabIndex: 0 };
  }
  render(){

    return(
      <TabStyle>
        <Tabs selectedIndex={this.state.tabIndex} onSelect={tabIndex => this.setState({ tabIndex })}>
          <TabList>
              <Tab>Tab 1</Tab>
              <Tab>Tab 2</Tab>
            </TabList>
          
          <section>
            <TabPanel>
            <h2>Tab 1</h2>
              <p>There are many ways to make the height of your HTML elements to be exactly as the height of the browser screen, regardless of the screen resolution, browser, device, etc. Many of these options use JavaScript to find out the height of the window. But there’s a way to do this with pure CSS only.</p>
            </TabPanel>     
            <TabPanel>
              <h2>Tab 2</h2>
              <p>The viewport-percentage lengths are relative to the size of the initial containing block. When the height or width of the initial containing block is changed, they are scaled accordingly.</p>
            </TabPanel>
          </section>

        </Tabs>
      </TabStyle>
    )
  }
}

export default TabsComp