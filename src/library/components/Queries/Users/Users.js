import React, { Component, Fragment } from 'react'
import { gql } from 'apollo-boost'
import { graphql } from 'react-apollo'
import styled from 'styled-components'

import CreateUser from './CreateUser'

//  Styles 

const Article = styled.article`
  background:#ddd;
  margin: 20px  0px;
  padding:20px;
  color:#222;
`;

// GraphQL Query

const getUsersQuery = gql `
  {
    users{
      username,
      email,
      id
    }
  }
`
// Components 



// import { FormElements } from  '../../library/components/FormElements'

class Users extends Component {

  displayUsers(){
    let data = this.props.data
    console.log(data.users)
    if(data.loading){
      return (<div>Loading Users...</div>)
    }
    else{
      return data.users.map(user => {
        return(
            <Article key={user.id} className="user-list">
               <h3>{user.username}</h3>
                <p className="client-uri">{user.email}</p>
            </Article>
        )
      })
    }
  }
  render() {
    return (
      <Fragment>
        <div className="Users">
          <h2>Users</h2>
          { this.displayUsers() }
        </div>

        <CreateUser/>

      </Fragment>
    )
  }
}

export default graphql(getUsersQuery)(Users)
