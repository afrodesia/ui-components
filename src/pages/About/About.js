import React, { Component, Fragment } from 'react'
import { Helmet } from 'react-helmet'

import { TabsComp } from  '../../library/components/TabsComp'
import { AccordianView } from  '../../library/components/Accordian'

// import { NewsList } from  '../../library/components/News'
class About extends Component {
  render() {
    return (
      <Fragment>
        <Helmet>
        	<title> About - UI Components </title>
          <meta property="og:title" content="About" />
          <meta name="description" content="This is the About section" />
      	</Helmet>
        <div className="Contact container">
          <h1>About</h1>
        
           <TabsComp/>
           <AccordianView/>
        </div>
      </Fragment>
    )
  }
}

export default About