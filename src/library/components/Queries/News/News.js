import React from 'react';

import { NewsList } from './'

const News = (props) => {
  return(
    <div className="News">
      <h2>News</h2>
      <NewsList/>
    </div>
  )
}

export default News; 