const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLSchema,
  GraphQLID

} = require('graphql')

const ClientType = new GraphQLObjectType({
  name: 'Client',
  fields: () => ({
    id: { type: GraphQLID },
    name: { type:GraphQLString },
    uri: { type:GraphQLString },
    description: { type:GraphQLString }
  })
})

module.exports = {
  ClientType
}
