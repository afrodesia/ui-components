const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLSchema,
  GraphQLID

} = require('graphql')


const NewsType = new GraphQLObjectType({
  name: 'News',
  fields: () => ({
    id: { type: GraphQLID },
    title: { type:GraphQLString },
    imageLink: { type:GraphQLString },
    details: { type:GraphQLString }
  })
})

module.exports = {
  NewsType
}
