import React, { Fragment } from 'react'
import styled from 'styled-components'


const Table= styled.table`
  background:#ddd;
  width:100%;
  th{
    text-align:left;
    padding:20px;
    border:2px solid #222;
    background:#222;
    color:#fff;

  }
  tr{

  }
  td{
    border:2px solid #222;
    padding:20px;
  }

`;
const CoinTable = () => {
  return(
    <Fragment>
        <Table>
          <tr>
            <th>Company</th>
            <th>Contact</th>
            <th>Country</th>
          </tr>
          <tr>
            <td>Alfreds Futterkiste</td>
            <td>Maria Anders</td>
            <td>Germany</td>
          </tr>
          <tr>
            <td>Centro comercial Moctezuma</td>
            <td>Francisco Chang</td>
            <td>Mexico</td>
          </tr>
          <tr>
            <td>Ernst Handel</td>
            <td>Roland Mendel</td>
            <td>Austria</td>
          </tr>
          <tr>
            <td>Island Trading</td>
            <td>Helen Bennett</td>
            <td>UK</td>
          </tr>
          <tr>
            <td>Laughing Bacchus Winecellars</td>
            <td>Yoshi Tannamuri</td>
            <td>Canada</td>
          </tr>
          <tr>
            <td>Magazzini Alimentari Riuniti</td>
            <td>Giovanni Rovelli</td>
            <td>Italy</td>
          </tr>
        </Table>

    
    </Fragment>
  )
}

export default CoinTable