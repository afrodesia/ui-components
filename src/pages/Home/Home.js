import React, { Component, Fragment } from 'react'
import { Helmet } from 'react-helmet'
import styled from 'styled-components'

import Masthead from './components/Masthead'

//  Styles 

const HomeBg = styled.div`
  width:100%;
  height:82.6vmin;
  background:#222;
  display:flex;
  align-items: center;
  justify-content: center;

  div{
    max-width: 50%;
    color:#fff;
  }
  h1{
    font-weight:900;
  }
  p{
    padding-top:20px;
    text-align:center;
    clear:both;
    
  }
  @media only screen and (max-width: 800px){
    height:100vh;
    div{
      max-width: 80%;
    }
    h1{
      font-size:2em;
      text-align:center;
    }
  }
`;

// Components 

// import { FormElements } from  '../../library/components/FormElements'
// import { DataTable} from  '../../library/components/DataTable'

class Home extends Component {
  render() {
    return (
      <Fragment>
        <Helmet>
        	<title> Home - UI Components </title>
          <meta property="og:title" content="Home" />
          <meta name="description" content="This is the Home section" />
      	</Helmet>
        <HomeBg>
          <Masthead />    
        </HomeBg>
      </Fragment>
    )
  }
}

export default Home
