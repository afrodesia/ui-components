import Users from './Users/Users'
import Clients from './Clients/Clients'
import NewsList from './News/NewsList'

export{
  Users,
  Clients,
  NewsList
}