import React, { Component } from 'react'

export default class FilterForm extends Component {
  constructor(props){
    super(props)
    this.state = {
      postFilter: ""
    }
  }
  handleChange = (e) => {
    this.setState({
      postFilter : e.target.value
    })
    this.props.onChange(e.target.value)
  }
   render() {
    return (
      <div> 
        <label htmlFor="filter">Filter by Post: </label>
        <input 
          type="text" 
          id="filter" 
          value={this.state.postFilter}
          onChange={this.handleChange} 
          />
      </div>
    )
  }
}
