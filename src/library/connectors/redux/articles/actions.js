import {ADD_ARTICLE} from './actionTypes'

export const addArticle = article => ({
  type: "ADD_ARTICLE",
  payload: article 
})