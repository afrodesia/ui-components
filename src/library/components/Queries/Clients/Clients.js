import React, { Component, Fragment } from 'react'
import { gql } from 'apollo-boost'
import { graphql } from 'react-apollo'
import styled from 'styled-components'

//  Styles 

const Article = styled.article`
  background:#ddd;
  margin: 20px  0px;
  padding:20px;
  color:#222;
`;

// GraphQL Query

const getClientsQuery = gql `
  {
    clients{
      name,
      uri,
      description,
      id
    }
  }
`
// Components 



class Clients extends Component {

  displayClients(){
    let data = this.props.data
    console.log(data.clients)
    if(data.loading){
      return (<div>Loading Clients...</div>)
    }
    else{
      return data.clients.map(client => {
        return(
            <Article key={client.id} className="client-list">
               <h3>{client.name}</h3>
                <p className="client-uri">{client.uri}</p>
                <p className="client-desc">{client.description}</p>
            </Article>
        )
      })
    }
  }
  render() {
    return (
      <Fragment>
        <div className="Clients">
          <h2>Clients</h2>          
          { this.displayClients() }
        </div>
      </Fragment>
    )
  }
}

export default graphql(getClientsQuery)(Clients)
