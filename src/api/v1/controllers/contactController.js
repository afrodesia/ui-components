const Contact = require('../models/Contact')

module.exports = {

	find: (params, callback) => {
		Contact.find(params, (err,contacts) => {
			if(err){
				callback(err, null)
				return
			}
			callback(null, contacts)
		})
	},
	findById: (id, callback) => {
		Contact.findById(id, (err, contact) => {
			if (err){
				callback(err, null)
				return
			}
			callback(null, contact)
		})
	},
	create: (params,callback)=> {

		Contact.create(params, (err, contact) => {
			if (err){
				callback(err, null)
				return
			}
			callback(null, contact)
		})
	},
	update: (id, params, callback) => {
		Contact.findByIdAndUpdate(id,params, {new:true}, (err, contact) => {
			if(err){
				callback(err, null)
				return
			}
			callback(null, contact)

		})

	},	
	destroy: (id, callback) => {
		Contact.findByIdAndRemove(id, (err) => {
			if(err){
				callback(err, null)
				return
			}
			callback(null, contact)
		})
	}
}