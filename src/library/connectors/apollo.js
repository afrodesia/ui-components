import { ApolloClient } from 'apollo-boost'
import { HttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'
import fetch from 'node-fetch'

const client = new ApolloClient({
  ssrMode: true,
  link: new HttpLink({
    fetch,
    uri: 'api/v2/graphql',
    credentials: 'same-origin',
  }),
  cache: new InMemoryCache(),
  ssrForceFetchDelay: 100,
})

export default client