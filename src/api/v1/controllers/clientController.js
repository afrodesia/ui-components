const Client = require('../models/Client')

module.exports = {
  find: (params, callback) => {
    Client.find(params, (err, clients) => {
      if(err){
        callback(err, null)
        return
      }
      callback(null, clients)
    })
  },
  findById: (id, callback) => {
    Client.findById(id, (err, client) => {
      if(err){
        callback(err, null)
        return
      }
      callback(null, client)
    })

  },
  create: (params, callback) => {
    Client.create(params, (err, client) => {
      if(err){
        callback(err, null)
        return
      }
      callback(null, client)
    })
  },
  update: () => {

  },
  destroy: () => {

  }
}
