import React, { Component, Fragment } from 'react'
import { Helmet } from 'react-helmet'

// import { FormElements } from  '../../library/components/FormElements'
// import { DataTable} from  '../../library/components/DataTable'
import UseIt from "./UseIt"
class Elements extends Component {
  render() {
    return (
      <Fragment>
        <Helmet>
        	<title> Elements- UI Components </title>
          <meta property="og:title" content="Home" />
          <meta name="description" content="This is the Home section" />
      	</Helmet>
        <div className="Elements container">
          <UseIt/>
        </div>
      </Fragment>
    )
  }
}

export default Elements
