import React, { Component, Fragment } from 'react'
import { Helmet } from 'react-helmet'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs'

import { Clients, NewsList, Users } from  '../../library/components/Queries'
import styled from 'styled-components'

//  Styles 

const TabView = styled.div`
  .react-tabs__tab-list{
    margin:30px 0px;
    width:100%;
    display:block;
    li{
      background:#333;
      display:inline-block;
      padding:10px;
      margin-right:2px;
      color:#fff;
      cursor:pointer;
    }
  }
`;





class Queries extends Component {

  constructor() {
    super();
    this.state = { tabIndex: 0 };
  }

  render() {
    return (
      <Fragment>
        <Helmet>
        	<title> Queries - UI Components </title>
          <meta property="og:title" content="Queries" />
          <meta name="description" content="This is the Queries section" />
      	</Helmet>
        <div className="Queries container">
          <h1>Queries</h1>

          <Tabs selectedIndex={this.state.tabIndex} onSelect={tabIndex => this.setState({ tabIndex })}>

          <TabView>
            <TabList>
              <Tab>Users</Tab>
              <Tab>News</Tab>
              <Tab>Clients</Tab>
            </TabList>
          </TabView>

          <section>
            <TabPanel>
                <Users/>
            </TabPanel>     
            <TabPanel>
                <NewsList/>
            </TabPanel>
            <TabPanel>
                <Clients/>
            </TabPanel>
          </section>

        </Tabs>

        </div>
      </Fragment>
    )
  }
}

export default Queries