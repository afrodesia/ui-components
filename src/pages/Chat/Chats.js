import React, { Component, Fragment } from 'react'
import { Helmet } from 'react-helmet'

// import { FormElements } from  '../../library/components/FormElements'
// import MessageApp  from  '../../library/components/Chat/index'
class Chats extends Component {
  render() {
    return (
      <Fragment>
        <Helmet>
        	<title> Chats- UI Components </title>
          <meta property="og:title" content="Home" />
          <meta name="description" content="This is the Home section" />
      	</Helmet>
        <div className="Chats container">

        </div>
      </Fragment>
    )
  }
}

export default Chats
