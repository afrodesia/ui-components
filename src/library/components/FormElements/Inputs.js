import React from 'react'


import './inputs.css'

const Inputs = (props) => {
  return(
    <input 
      type={props.type}
      value={props.value}
      placeholder={props.placeholder}
      onChange={props.handleChange}
    />
  )
}


export default Inputs