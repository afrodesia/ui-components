import { POST_PROJECT, DELETE_PROJECT } from '../constants'

export default function(state = [], action) {
	const { type, text, project} = action

	switch (type) {
		case POST_PROJECT:
			return [
				...state,
				{
					text
				}
			]
		case DELETE_PROJECT:
			return state.filter(i => i !== project)
		default:
			return state
	}
}
