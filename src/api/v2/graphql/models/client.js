const mongoose = require('mongoose')
const Schema = mongoose.Schema

const ClientSchema = new Schema({
  name: String,
  uri: String,
  description: String
})

module.exports = mongoose.model('Client', ClientSchema)