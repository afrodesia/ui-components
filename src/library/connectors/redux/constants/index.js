export const URL = 'http://localhost:8000'
// LOGIN
export const LOGGED_IN = 'LOGGED_IN'
export const INPUT_VALUE = 'INPUT_VALUE'
// TODOS
export const ADD_TODO = 'ADD_TODO'
export const REMOVE_TODO = 'REMOVE_TODO'

export const POST_PROJECT = 'POST_PROJECT'
export const DELETE_PROJECT = 'DELETE_PROJECT'

