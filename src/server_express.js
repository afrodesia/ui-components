import App from './App'
import React from 'react'
import { StaticRouter } from 'react-router-dom'
import express from 'express'
import graphqlHTTP from 'express-graphql'
import { Helmet } from 'react-helmet'
import { renderToString } from 'react-dom/server'
import mongoose from 'mongoose'
import bodyParser from 'body-parser'
import schema from './api/v2/graphql'

import { ApolloProvider, getDataFromTree } from 'react-apollo'

import client from './library/connectors/apollo'

console.log(client)

const assets = require(process.env.RAZZLE_ASSETS_MANIFEST)
const server = express()

// Database Connection
const MONGO_URI = 'mongodb://localhost/ui-components'
mongoose.connect(MONGO_URI, function(err, res){
  if (err){
    console.log('DB CONNECTION FAILED')
  }
  else {
    console.log('DB CONNECTION ESTABLISHED!')
  }
})

// Render 
server.use(bodyParser.json())
server.use(bodyParser.urlencoded({ extended: false }))
server.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
  next()
})

 // API Resource Routes
// import api from'./api/v1/routes/api.js'

// server.use('/api/v1', api , (req, res, next) => {
//   res.header("Access-Control-Allow-Origin", "*")
//   res.header(
//     "Access-Control-Allow-Headers",
//     "Content-Type, Authorization, Content-Length, X-Requested-With"
//   )
//   if (req.method === "OPTIONS") {
//     res.sendStatus(200);
//   } else {
//     next()
//   }
// })

// Graphql Schema

server.use('/api/v2/graphql', graphqlHTTP({
  schema,
  graphiql: true
}))

// React.js Powered Routes
server
  .disable('x-powered-by')
  .use(express.static(process.env.RAZZLE_PUBLIC_DIR))
  .get('/*', (req, res) => {
    const context = {}
    // const client = client;
   
    const helmet = Helmet.renderStatic()
    const markup = renderToString(
      <ApolloProvider client={client}>
        <StaticRouter context={context} location={req.url}>
          <App />
        </StaticRouter>
      </ApolloProvider>
    );

    if (context.url) {
      res.redirect(context.url);
    } else {
      res.status(200).send(
`<!doctype html>
  <html lang="">
    <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="HandheldFriendly" content="true">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
    ${helmet.meta.toString()}
    ${helmet.title.toString()}  
      ${
        assets.client.css
          ? `<link rel="stylesheet" href="${assets.client.css}">`
            : ''
        }
      ${
        process.env.NODE_ENV === 'production'
          ? `<script src="${assets.client.js}" defer></script>`
            : `<script src="${assets.client.js}" defer crossorigin></script>`
        }
    </head>
    <body>
      <div id="root">${markup}</div>
    </body>
  </html>`
      );
    }
  });

export default server;
