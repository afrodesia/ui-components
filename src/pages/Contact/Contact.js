import React, { Component, Fragment } from 'react'
import { Helmet } from 'react-helmet'
// import { Switch, Route} from 'react-router-dom'


import { FilterForm } from  '../../library/components/Filter'

// import { Users, Clients, NewsList } from  '../../library/components/Queries'

class Contact extends Component {


  render() {
    return (
      <Fragment>
        <Helmet>
        	<title> Contact - UI Components </title>
          <meta property="og:title" content="Home" />
          <meta name="description" content="This is the Contact section" />
      	</Helmet>
        <div className="Contact container">
          <h1>Contact</h1>
          <FilterForm />
        </div>
      </Fragment>
    )
  }
}

export default Contact
