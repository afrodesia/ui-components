const mongoose = require('mongoose')

const ContactSchema = new mongoose.Schema({
  name: { type:String, default:'' },
  email: { type:String, default:'' },
  body: { type:String, default:'' },
  date: { type:Date, default:Date.now }  
})

module.exports = mongoose.model('ContactSchema', ContactSchema)