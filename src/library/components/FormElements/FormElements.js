import React, {Component} from 'react'

import { Inputs } from './'

class FormElements extends Component {
  constructor(){
    super()
    this.state = {
       email: '',
       password: ''
    }
    this.handleChange = this.handleChange.bind(this)
  }
  

  handleChange = (e, attr) => {
    const newState  = { ...this.state}
    newState[attr] = e.target.value
    this.setState(newState)
  }
  render(){
    return(
      <form>
        FormElements
        <Inputs 
          placeholder="email" 
          type="email" 
          value={ this.state.email }
          handleChange={(e) => this.handleChange(e, 'email')}
          />
        <Inputs 
          placeholder="password" 
          type="password"
          value={this.state.password}
          handleChange={(e) => this.handleChange(e, 'password')}
          />
      </form>
    )
  }
}

export default FormElements