// SCHEMA FILE 
const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLSchema,
  GraphQLID,
  GraphQLList

} = require('graphql')
const _ = require('lodash')

// MODELS
const Client = require('./models/client')
const User = require('./models/user')
const News = require('./models/news')



// Types
// const { NewsType, ClientType, UserType  } = require('./types')

const NewsType = new GraphQLObjectType({
  name: 'News',
  fields: () => ({
    id: { type: GraphQLID },
    title: { type:GraphQLString },
    imageLink: { type:GraphQLString },
    details: { type:GraphQLString }
  })
})

const ClientType = new GraphQLObjectType({
  name: 'Client',
  fields: () => ({
    id: { type: GraphQLID },
    name: { type:GraphQLString },
    uri: { type:GraphQLString },
    description: { type:GraphQLString }
  })
})
const UserType = new GraphQLObjectType({
  name: 'User',
  fields: () => ({
    id: { type: GraphQLID },
    username: { type:GraphQLString },
    email: { type:GraphQLString }
  })
})

// ROOT QUERY

const RootQuery = new GraphQLObjectType({
  name: 'RootQueryType',
  fields: {

    // NEW
    new: {
      type: NewsType,
      args: { id: { type: GraphQLID }},
      resolve(parent, args){
        // Code to get data from DB
        // return _.find(clients, { id: args.id })
        return News.findById(args.id)
      }
    },
    // NEWS
    news: {
      type: new GraphQLList(NewsType),
      resolve(parent, args){
        // Code to get data from DB
        return News.find({})
      }
    },
    // CLIENT
    client: {
      type: ClientType,
      args: { id: { type: GraphQLID }},
      resolve(parent, args){
        // Code to get data from DB
        // return _.find(clients, { id: args.id })
        return Client.findById(args.id)
      }
    },
    // CLIENTS
    clients: {
      type: new GraphQLList(ClientType),
      resolve(parent, args){
        // Code to get data from DB
        return Client.find({})
      }
    },
    // USER
    user: {
      type: UserType,
      args: { id: { type: GraphQLID }},
      resolve(parent, args){
        // Code to get data from DB
        // return _.find(users, { id: args.id })
        return User.findById(args.id)
      }
    },
    // USERS
    users: {
      type: GraphQLList(UserType),
      resolve(parent, args){
        // Code to get data from DB
        // return users
        return User.find({})
      }
    }
  }
})

const Mutation = new GraphQLObjectType({
  name: 'Mutation',
  fields: {
    // ADD CLIENT
    addClient: {
      type:ClientType,
      args:{
        name: { type: GraphQLString },
        uri: { type: GraphQLString },
        description: { type: GraphQLString}
      },
      resolve(parent, args){
        let client = new Client({
          name: args.name,
          uri: args.uri,
          description: args.description
        })
        return client.save()
      }
    },
    // ADD USER
    addUser: {
      type:UserType,
      args:{
        username: { type: GraphQLString },
        email: { type: GraphQLString }
      },
      resolve(parent, args){
        let user = new User({
          username: args.username,
          email: args.email
        })
        return user.save()
      }
    },
    // ADD News
    addNews: {
      type:NewsType,
      args:{
        title: { type: GraphQLString },
        imageLink: { type: GraphQLString },
        details: { type: GraphQLString },

      },
      resolve(parent, args){
        let news = new News({
          title: args.title,
          imageLink: args.imageLink,
          details: args.details
        })
        return news.save()
      }
    }
  }
})

module.exports = new GraphQLSchema({
  query: RootQuery,
  mutation: Mutation
})

