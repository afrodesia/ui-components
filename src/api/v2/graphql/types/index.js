const ClientType = require('./client')
const UserType = require('./user')
const NewsType = require('./news')

module.exports = {
  ClientType, 
  UserType,
  NewsType
}