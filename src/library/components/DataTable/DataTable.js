import React, { Fragment } from 'react'

import { CoinTable, DataList } from './'


const DataTable = () => {
  return(
    <Fragment>
      <h1>Data Table</h1>
        
        <CoinTable/>
        <DataList/>
    </Fragment>
  )
}

export default DataTable