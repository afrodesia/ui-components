import React from 'react'
import { Switch, Route } from 'react-router-dom'

// COMPONENTS
import Nav from './library/components/Nav/Nav'  

// PAGES
import Home from './pages/Home/Home'
import Elements from './pages/Elements/Elements'
import About from './pages/About/About'
import Chats from './pages/Chat/Chats'
import Contact from './pages/Contact/Contact'
import Queries from './pages/Queries/Queries'
// APP STYLES
import './App.scss'

const App = () => (
  <div>
    <Nav/>
    <Switch>
      <Route exact path="/" component={Home} />
      <Route exact path="/elements" component={Elements} />
      <Route exact path="/about" component={About} />
      <Route exact path="/queries" component={Queries} />
      <Route exact path="/chats" component={Chats} />
      <Route exact path="/contact" component={Contact} />
    </Switch>
  </div>
)

export default App
