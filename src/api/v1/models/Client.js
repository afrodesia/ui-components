const mongoose = require('mongoose')

const ClientSchema = new mongoose.Schema({
  client: { type:String, default:'' },
  url: { type:String, default:'' },
  logo: { type:String, default:'' },
  about: { type:String, default:'' }  
})

module.exports = mongoose.model('ClientSchema', ClientSchema)