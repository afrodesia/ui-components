import React, { Component } from 'react'
import { graphql } from 'react-apollo'
import gql from 'graphql-tag'

const CREATEUSER_MUTATION = gql`

  mutation createUserMutation(
      $username: String, 
      $email: String!
    ) {
    createUser(username: $username, email: $email) {
      id
      username
      email
    }
  }
`


class CreateUser extends Component{
  state = {
    username: '',
    email: '',
  }
  addUser = async (e) =>{
    e.preventDefault()
    const { username, email } = this.state
      await this.props.createUserMutation({
        variables: {
          username,
          email
        }
      })
  }
  render(){
    return(
       <form>
          <div className="form-group">
            <input
              value={this.state.username}
              onChange={ (e) => this.setState({ username: e.target.value })}
              type="text"
            />
            <label>Add A User</label>
          </div>
          <div className="form-group">
            <input
              value={this.state.email} 
              onChange={ (e) => this.setState({ email: e.target.value })}
              type="text"
            />
            <label>Add A Email</label>
          </div>
          <button onClick={(e) => this.addUser()}>Add User</button>
       </form>
    )
  }
}

export default graphql(
    CREATEUSER_MUTATION, { name: 'createUserMutation' })(CreateUser)
    