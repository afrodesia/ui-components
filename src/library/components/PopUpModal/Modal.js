import React from "react";
import { Link } from 'react-router-dom'
import Modal from "react-responsive-modal";

//  Styles 

const styles = {
  fontFamily: "sans-serif",
  textAlign: "center"
};

class PopUpModal extends React.Component {
  state = {
    open: false
  };

  onOpenModal = () => {
    this.setState({ open: true });
  };

  onCloseModal = () => {
    this.setState({ open: false });
  };

  render() {
    const { open } = this.state;
    return (
      <div style={styles}>
        <Modal className="modal-form" open={open} onClose={this.onCloseModal} little>
          <h2>Form</h2>
           <form>
            <div className="form-group">       
              <input
                name="email"
                type="email"
              />
              <label>Email</label>
            </div>
            
            <div className="form-group">       
              <input
                name="password"
                type="password"
              />
              <label>Password</label>
            </div>
            
            <div className="form-group">       
              <button className="btn">Submit</button>
            </div>
          </form>
        </Modal>
      </div>
    );
  }
}

export default PopUpModal