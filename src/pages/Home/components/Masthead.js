import React from 'react';

const Masthead = () => {
  return (
    <div>
      <h1>UI Components</h1>
      <p>A repository of react components</p>  
    </div>
  );
};

export default Masthead;