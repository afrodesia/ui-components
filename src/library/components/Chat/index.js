import React, { Component } from "react"

import Chat from './Chat'

class MessageApp extends Component {
  state = { isStreaming: false };
  handleStop = () => {
    this.setState({ isStreaming: false });
  };
  render() {
    return (
      <section style={{width: "75vw", margin: "0 auto"}}>
        <Chat isStreaming={this.state.isStreaming} />
        <button style={{ float: "right" }} onClick={this.handleStop}>
          Stop
        </button>
      </section>
    );
  }
}

export default MessageApp