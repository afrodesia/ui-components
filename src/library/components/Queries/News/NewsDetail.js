import React from 'react';

const NewsDetail = (props) => {
  return(
    <article className="news-article">
      <div className="image">
        <img src={props.image.imageLink} alt={props.image.title}/>
      </div>
      <div className="details">
        <h4>{props.image.title}</h4>
        <p>{props.image.details}</p>
      </div>
    </article>
  )
}

export default NewsDetail; 