const Article = require('../models/Article')

module.exports = {

	find: (params, callback) => {
		Article.find(params, (err,articles) => {
			if(err){
				callback(err, null)
				return
			}
			callback(null, articles)
		})
	},

	findById: (id, callback) => {
		Article.findById(id, (err, article) => {
			if (err){
				callback(err, null)
				return
			}
			callback(null, article)
		})
	},

	create: (params,callback) => {
		Article.create(params, (err, article) => {
			if (err){
				callback(err, null)
				return
			}
			callback(null, article)
		})
	},
	update: (id, params, callback) => {
		Article.findByIdAndUpdate(id,params, { new:true }, (err, article) => {
			if(err){
				callback(err, null)
				return
			}
			callback(null, article)
		})
	},

	destroy: (id, callback) => {
		Article.findByIdAndRemove(id, (err) => {
			if(err){
				callback(err, null)
				return
			}
			callback(null, article)
		})
	}
}