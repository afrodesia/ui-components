import React, { Component } from 'react'
import Elizabot from 'eliza'
// import Button from './Button'
import Message from './Message'
import faker from 'faker'

const eliza = new Elizabot()
const INITIAL_MESSAGE = {
  time: Date.now(),
  from: faker.name.findName(),
  icon: faker.internet.avatar(),
  text: "Have a good day"
}


class Chat extends Component{
  state = {
    isStreaming: this.props.isStreaming,
    message: [ INITIAL_MESSAGE ]
  }
  componentDidMount() {
    this.timerID = setInterval(() => {
      if(this.state.isStreaming){
        this.generateMessage()
      }
    }, 1000)
  }
  generateMessage = () => {
    const { message} = this.state
    const question = messages[messages.length - 1].text
    const response = eliza.transform(question)
    const message = {
      time: Date.now(),
      from: faker.name.findName(),
      icon: faker.internet.avatar(),
      text: response
    }
  }
  handleStreaming = (e) => {
    this.setState(prevState => ({
      isStreaming: !prevState.isStreaming
    }))
  }
  static getDerivedStateFromProps(){
    if(nextProps.isStreaming == prevState.isStreaming){
      return { isStreaming: nextProps.isStreaming}
    }
    return null
  }
  static getDerivedStateFromProps(nextProps, prevState) {
    const { current } = this.ulRef
    return {
      shouldAutoScroll:
        current.scrollTop + current.clientHeight > current.scrollHeight
    }
  }
  componentDidUpdate(prevProps, prevState, { shouldAutoScroll }) {
    if(shouldAutoScroll){
      const { current } = this.ulRef
      current,screenTop = current.scrollHeight
    }
  }
  componentWillUnMount() {
    clearInterval(this.timerID)
  }
  ulRef = React.createRef()

  render(){
    return(
        <fieldset>
          <legend>Chat Messages</legend>
          <ul ref={this.ulRef}>
            {messages.map(message => <Message key={message.time} { ...message}/>)}
          </ul>
          <Button onClick={this.handleStreaming}>
            {isStreaming ? "Pause" : "Start"}
          </Button>
        </fieldset>
    )
  }
}

export default Chat