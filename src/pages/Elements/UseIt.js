import React, { useState, useEffect, Fragment } from 'react'

const UseIt = (props) => {
  const [ count, setCount ] = useState(0)
  const [ isOn, setIsOn] = useState(false)

  useEffect(() => {
    document.title = `You have clicked ${count} times`
  })

  const incrementCount = () => {
    setCount(prevCount => prevCount + 1)
  }
  const toggleLight = () => {
    setIsOn(prevIsOn => !prevIsOn)
  }
  
  return (

    <Fragment>
      <button onClick={incrementCount}>
        I was clicked { count } times
      </button>

      <h2>Toggle</h2>
      <img
        src={
          isOn 
          ? 'https://icon.now.sh/highlight/fd0'
          : 'https://icon.now.sh/highlight/aaa'
        }
        style={{
          height: "100px",
          width: "100px",
          color: isOn ? "yellow" : "grey",
          cursor: "pointer"
        }}
        onClick={toggleLight}
        alt="flashlight"
      />



    </Fragment>
  )

}


export default UseIt