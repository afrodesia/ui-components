import React, { Fragment } from 'react'
import styled from 'styled-components'


const DataListForm = styled.form`
  width:100%;
  margin:20px 0px;
  input{
    padding:10px;
    width:50%;
    margin-right:0px;
  }
  datalist{
    background:#333;
    option{
      border: red solid 2px;
    }
  }
  input[type=submit] {
      background:#222;
      width:150px !important;
      box-sizing: border-box;
      color:#fff;
      padding:10px;
      border-top:1px solid #222;
      margin-left:-3px;
      outline:0;
      cursor:pointer;
  }
`;
const DataList = () => {
  return(
    <Fragment>
        <DataListForm action="/" method="get">
          <input list="browsers" name="browser"/>
          <datalist id="browsers">
            <option value="Internet Explorer"/>
            <option value="Firefox"/>
            <option value="Chrome"/>
            <option value="Opera"/>
            <option value="Safari"/>
          </datalist>
          <input type="submit"/>
        </DataListForm>

    </Fragment>
  )
}

export default DataList