import React from 'react'

const Message = (props) => {
  return(
    <li>
      <img src={props.icon} />
      <div>
        <span>{props.from}</span>
        <span>{new Date(time).toLocaleTimeString()}</span>
      </div>
      <div>{props.text}</div>
    </li>
  )
}

export default Message