import News from './News';
import NewsList from './NewsList';
import NewsDetail from './NewsDetail';

export{
  News,
  NewsList,
  NewsDetail,
}