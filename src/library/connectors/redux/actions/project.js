import { POST_PROJECT, DELETE_PROJECT } from './constants'

export function addTodo(text) {
	return {
		type: POST_PROJECT,
		text
	}
}

export function removeTodo(project) {
	return {
		type: DELETE_PROJECT,
		project
	}
}
