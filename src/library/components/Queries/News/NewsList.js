import React, { Component, Fragment  } from 'react';
import { gql } from 'apollo-boost'
import { graphql } from 'react-apollo'
// import { NewsDetail } from './'



// GraphQL Query

const displayNewsQuery = gql `
  {
    news{
      title,
      imageLink,
      details,
      id
    }
  }
`;


class NewsList extends Component {

  displayNews(){
    let data = this.props.data
    console.log(data.news)
    if(data.loading){
      return (<div>Loading News...</div>)
    }
    else{
      return data.news.map(newer => {
        return(
            <article key={newer.id} className="user-list">
               <h3>{newer.title}</h3>
               <img src={`./images/${newer.imageLink}`} alt={newer.title}/>
                <p className="client-uri">{newer.details}</p>
            </article>
        )
      })
    }
  }
  render() {
    return (
      <Fragment>
        <div className="News">
          <h2>News</h2>
          { this.displayNews() }
        </div>
      </Fragment>
    )
  }
}

export default graphql(displayNewsQuery)(NewsList)