import React, { Component } from "react";

import './scss/MenuButton.scss'

export default class MenuButton extends Component {
  render() {
    return (
      <button 
        id="roundButton"
        onMouseUp={this.props.handleMouseUp}>
        menu
      </button>
    );
  }
}
 
