import React from 'react'
import { hydrate } from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import { Provider } from 'react-redux'
import { ApolloProvider } from 'react-apollo'

import client from './library/connectors/apollo'
import store from './library/connectors/redux'
import App from './App'

console.log(client)

hydrate(
  <ApolloProvider client={client}>
    <Provider store={store}>
      <BrowserRouter>
        <App />
      </BrowserRouter>
   </Provider>
  </ApolloProvider>
  ,
  document.getElementById('root')
);

if (module.hot) {
  module.hot.accept()
}
