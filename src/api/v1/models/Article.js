const mongoose = require('mongoose')

const ArticleSchema = new mongoose.Schema({
  title: { type:String, default:'' },
  image: { type:String, default:'' },
  content: { type:String, default:'' },
  date: { type:Date, default:Date.now }  
})

module.exports = mongoose.model('ArticleSchema', ArticleSchema)