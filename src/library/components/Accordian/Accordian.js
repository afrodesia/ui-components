import React, { Fragment } from 'react'
import Collapsible from 'react-collapsible'
import {
    AccordionWithHeader,
    AccordionNode,
    AccordionHeader,
    AccordionPanel
} from 'react-accordion-with-header'
  // import styled from 'styled-components'


// const AccordianStyles = styled.div`

// `;


const AccordianView = () => {
  return(
    <Fragment>
      <Collapsible trigger="Start here">
        <p>This is the collapsible content. It can be any element or React component you like.</p>
        <p>It can even be another Collapsible component. Check out the next section!</p>
      </Collapsible>

       <AccordionWithHeader>
        {[1, 2, 3, 4,5].map((item, i) => {
          return (
            <AccordionNode key={i}>
              <AccordionHeader>
                <div>
                  <h2>Some title!</h2>
                </div>
              </AccordionHeader>
              <AccordionPanel>
                <section>
                  <header>Some body information etc</header>
                  <article>Interesting things...</article>
                </section>
              </AccordionPanel>
            </AccordionNode>
          );
        })}
      </AccordionWithHeader>
     
    </Fragment>
  )
}

export default AccordianView